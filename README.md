# APP_team

### 项目介绍
小组协作式Axure项目
中山大学南方学院<br> 
学院：文学与传媒学院<br>
专业：网络与新媒体专业<br> 
成员：童鼎、邱瑜安、林钰江、林立宇、刘启伦、黄创维、朱金鑫<br>
### 产品方向

#### 产品定位
资讯推送类APP

#### 产品核心目标
为游戏爱好者提供更全面、更多平台的游戏资讯推送

#### 产品核心功能
1. 为用户推送游戏相关资讯内容（包括游戏价格、游戏内容预告）
2. 为用户进行相对低价的游戏购买服务
3. 为用户添加合适当前游戏版本的优质攻略内容


#### 盈利点
低价入手游戏KEY，在游戏价格回调时再出手卖给玩家


### 目标市场

1.  Epic、origin等游戏平台玩家
2.  对游戏有兴趣但是无条件游玩的用户
3.  游戏相关从业人员

# 产品原型图
<img src="https://gitee.com/timem00n/app_-cms/raw/master/PIC/%E4%BA%A7%E5%93%81%E5%8E%9F%E5%9E%8B.jpg">


### 竞品分析

#### 市场现状
<img src="https://gitee.com/timem00n/app_-cms/raw/master/PIC/6.png">



#### 竞品分析项目

| 项目名称 | 版本号      |
|------|----------|
| 小黑盒  | v1.3.138 |
| 机核   | v2.11.7  |


### 小黑盒

#### 小黑盒竞品核心功能分析
1. 游戏资讯共享平台（论坛）
2. 游戏购买平台（只与steam平台互通）<img src="https://gitee.com/timem00n/app_-cms/raw/master/PIC/%E5%B0%8F%E9%BB%91%E7%9B%92%E7%95%8C%E9%9D%A2.jpg">


### 机核

#### 机核核心功能分析
1. 文字内容创作平台（论坛杂谈，可发布不限于游戏、小说等已有原创作品的同人作品，也可在该平台上发布原创内容）
2. 电台
3. 游戏评测

#### 机核APP界面示范 
<img src="https://gitee.com/timem00n/app_-cms/raw/master/PIC/%E6%9C%BA%E6%A0%B8%E7%95%8C%E9%9D%A2%E7%A4%BA%E8%8C%83.png">

### 后台角色权限分配
<img src="https://gitee.com/timem00n/app_team/raw/master/PIC/CMS.png">


#### 参与贡献
负责Axure前端界面：邱瑜安、林立宇、刘启伦、黄创维、朱金鑫<br>
负责issue统计提交：林钰江<br>
负责prd/mrd文档写作、readme写作：童鼎<br>
